# consultocratie Le Monde

Nettoyage et exemples d'utilisation, grâce à PostgreSQL, de [données publiées par journal « Le Monde »](https://www.lemonde.fr/les-decodeurs/article/2022/03/17/explorez-les-1-600-missions-des-cabinets-de-conseil-pour-l-etat-recensees-par-le-monde_6118001_4355770.html "Hover display") relatives à des missions de conseil commandées par des ministères en France

Source:[Commission d'enquête sur l’influence croissante des cabinets de conseil privés sur les politiques publiques](https://www.senat.fr/commission/enquete/2021_influence_des_cabinets_de_conseil_prives.html)
[Synthèse du rapport](http://www.senat.fr/rap/r21-578-1/r21-578-1-syn.pdf)

## Importation des données
1. Télécharger [le fichier CSV](https://www.lemonde.fr/les-decodeurs/article/2022/03/17/explorez-les-1-600-missions-des-cabinets-de-conseil-pour-l-etat-recensees-par-le-monde_6118001_4355770.html "Hover display") 

``` shell
wget https://assets-decodeurs.lemonde.fr/decodeurs/assets/[D%C3%A9codeurs]%20Liste%20des%20missions%20de%20conseil/[De%CC%81codeurs]_Consultocratie___la_liste_des_missions_-_Listes_des_missions.csv
```

2. Créer une base de données: `createdb consultocratie`

3. Se connecter à cette base: `psql consultocratie`

4. Créer une table

``` sql
create table consulto_import(source text,intitulé_court text,intitulé_long text,détails text,catégorie text,acheteur text,ministère_rattachement text,type_entité text,cabinet_attributaire text,année text,date_attribution text,montant text,taxé text,lien_source text);
```

5. Importer les données

```
\copy consulto_import from '[Décodeurs]_Consultocratie___la_liste_des_missions_-_Listes_des_missions.csv' csv header;
```

## Nettoyage des données

L'outil favori du gros des consultants est le tableur, et la qualité des données (y compris celles qui décrivent leurs missions) en pâtit.

``` sql
-- année ou date_attribution manquante
select année,date_attribution from consulto_import where année is null or date_attribution is null;

-- correction d'encodage incorrect
update consulto_import set année=regexp_replace(année, '[^0-9]+', '', 'g');

update consulto_import set année=null where année='';

update consulto_import set date_attribution=null where date_attribution ~ 'HM';
update consulto_import set date_attribution=null where date_attribution = ' /';
update consulto_import set date_attribution=null where btrim(date_attribution) = '';
update consulto_import set date_attribution= date_attribution||'-12-31' where length(date_attribution)=4;

update consulto_import set source = 'Inconnu' where source is null or source='';
update consulto_import set intitulé_court = 'Inconnu' where intitulé_court is null or intitulé_court='';
update consulto_import set intitulé_long = 'Inconnu' where intitulé_long is null or intitulé_long='';
update consulto_import set détails = '' where détails is null or détails='';
update consulto_import set catégorie = 'Inconnu' where catégorie is null or catégorie='';
update consulto_import set acheteur = 'Inconnu' where acheteur is null or acheteur='';
update consulto_import set ministère_rattachement = 'Inconnu' where ministère_rattachement is null or ministère_rattachement='';
update consulto_import set type_entité = 'Inconnu' where type_entité is null or type_entité='';
update consulto_import set cabinet_attributaire = 'Inconnu' where cabinet_attributaire is null or cabinet_attributaire='';
update consulto_import set lien_source = 'Inconnu' where lien_source is null or lien_source='';

-- ne doit trouver que 'HT' et 'TTC'
select distinct taxé from consulto_import;
update consulto_import set taxé=case when taxé='TTC' then 'false' else 'true' end;
alter table consulto_import alter column taxé set data type bool using taxé::bool;

update consulto_import set date_attribution=regexp_replace(date_attribution, '/', '-') ;
update consulto_import set date_attribution= (substring(date_attribution,7,4)||'-'||substring(date_attribution,4,2)||'-'||substring(date_attribution,1,2)) where date_attribution ~ '^[0-9]{2}-';
update consulto_import set date_attribution ='2016-01-01' where date_attribution = '2016-2020';
alter table consulto_import alter column date_attribution set data type date using date_attribution::date;
alter table consulto_import alter column année set data type smallint using année::smallint;

-- ne doit rien trouver
select * from consulto_import where extract(year from date_attribution)!=année;
update consulto_import  set montant = translate(montant,' ','');
alter table consulto_import alter column montant set data type float USING montant::double precision;

-- majuscule/minuscules en vrac
update consulto_import set source = btrim(lower(source));
update consulto_import set intitulé_court = btrim(lower(intitulé_court));
update consulto_import set intitulé_long = btrim(lower(intitulé_long));
update consulto_import set détails = btrim(lower(détails));
update consulto_import set catégorie = btrim(lower(catégorie));
update consulto_import set acheteur = btrim(lower(acheteur));
update consulto_import set ministère_rattachement = btrim(lower(ministère_rattachement));
update consulto_import set type_entité = btrim(lower(type_entité));
update consulto_import set cabinet_attributaire = btrim(lower(cabinet_attributaire));
```

## Fichiers publiés

1. Dump de la base PostgreSQL:
 - consultocratie_((date))_((heure CST)).pgdump
   Créé grâce à ```pg_dump -v --blobs --format=c -d consultocratie -f consultocratie_$(date +%Y%m%d_%H%M%S).pgdump```

2. Fichier CSV
 - consultocratie_.csv : dump au format CSV
   Créé grâce à ```\COPY consulto_import TO 'consultocratie.csv'  WITH DELIMITER ',' CSV HEADER;```

## Utilisation

### Au moyen de PostgreSQL

Exemples:
``` sql
-- montant total par acheteur
select acheteur,sum(montant) as total from consulto_import group by acheteur order by total desc;

-- montant total par cabinet
select left(cabinet_attributaire,80) as "cabinet(s)",sum(montant) as total from consulto_import group by "cabinet(s)" order by total desc;

-- montant total par ministère
select ministère_rattachement,sum(montant) as total from consulto_import group by ministère_rattachement order by total desc;

-- montant total par type d'entité
select type_entité,sum(montant) as total from consulto_import group by type_entité order by type_entité desc;

-- montant total par intitulé court
select intitulé_court,sum(montant) as total from consulto_import group by intitulé_court order by total desc;

-- montant total par cabinet et année
select left(cabinet_attributaire,80) as "cabinet(s)",année,sum(montant) as total,avg(montant)::integer as "moyenne arith" from consulto_import  group by cabinet_attributaire,année order by cabinet_attributaire,année desc nulls last;

-- montant total par cabinet, ministère et année
select left(cabinet_attributaire,80) as "cabinet(s)",ministère_rattachement,sum(montant) as total from consulto_import  group by cabinet_attributaire,ministère_rattachement order by "cabinet(s)",ministère_rattachement;

-- montant total des missions dont l'intitulé long contient «informati» ou «logiciel» ou...
select cabinet_attributaire,intitulé_court,sum(montant) as total from consulto_import where intitulé_long ~ 'informati' or intitulé_long ~ 'logiciel' or intitulé_long ~ 'numérique' or intitulé_long ~ 'électro' or intitulé_court ~ 'informati' or intitulé_court ~ 'logiciel' or intitulé_court ~ 'numérique' or intitulé_court ~ 'électro' and not intitulé_court ~ 'non informatique' group by cabinet_attributaire,intitulé_court order by total desc;

-- montant total des missions dont la liste des noms de cabinets contient 'kinsey', par acheteur
select acheteur,sum(montant) as somme from consulto_import where cabinet_attributaire ~ 'kinsey' group by acheteur order by somme desc;
```

### Au moyen d'un tableur

Télécharger le fichier CSV publié ici.

# Complément

Des [feuilles de calcul publiées](http://www.senat.fr/espace_presse/actualites/202203/influence_des_cabinets_de_conseil_prives_sur_les_politiques_publiques.html) par la Commission d'Enquête semblent plus complètes (et difficiles à exploiter).
